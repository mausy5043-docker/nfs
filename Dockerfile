FROM alpine:latest
LABEL maintainer="Mausy5043"

RUN apk update \
 && apk upgrade \
 && apk add --no-cache \
            bash \
            nfs-utils \
            tzdata \
 && mkdir -p /var/lib/nfs/rpc_pipefs \
 && mkdir -p /var/lib/nfs/v4recovery \
 && echo "rpc_pipefs /var/lib/nfs/rpc_pipefs rpc_pipefs defaults 0 0" >> /etc/fstab \
 && echo "nfsd       /proc/fs/nfsd           nfsd       defaults 0 0" >> /etc/fstab \
 && rm -rf /var/cache/apk/*

ENV TZ=Europe/Amsterdam

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime \
 && echo $TZ > /etc/timezone

ENV EXPORTS="/data"
ENV EXPORT_OPTIONS="rw,sync,subtree_check,no_acl,all_squash,anonuid=1000,anongid=1000,insecure"

VOLUME /data

COPY ["/configure-exports.sh","/"]
COPY ["/start-container.sh","/"]
RUN chmod 755 /configure-exports.sh \
 && chmod 755 /start-container.sh

EXPOSE 111/udp
EXPOSE 111/tcp
EXPOSE 2049/udp
EXPOSE 2049/tcp
EXPOSE 32765/tcp
EXPOSE 32765/udp
EXPOSE 32766/tcp
EXPOSE 32766/udp
EXPOSE 32767/tcp
EXPOSE 32767/udp

ENTRYPOINT ["/start-container.sh"]
